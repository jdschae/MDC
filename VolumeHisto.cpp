#include "TROOT.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TMath.h"
#include "TString.h"
#include "TStyle.h"
#include "TMath.h"

#include <iostream>
#include <string>

using namespace std;

const TString main_path = "/projecta/projectdirs/lz/data/mdc1-prod/LZAP-2.0.0-PHYSICS-2.0.0/";
const TString file_day = "lz_201707";
const TString file_day_end = "****_lzap.root";
const TString ttree_name = "Interactions";
const TString dir_day = "lz_mdc1_07-";
const TString dir_day_year = "-2017/";
const TString file_name = main_path + dir_day + file_day;

TChain* load_file() {
	TChain* t = new TChain(ttree_name);

	for (int i = 1; i <= 30; ++i) {
		char buf[3];
		sprintf(buf, "%02d", i);
		t->Add(main_path + dir_day + TString(buf) + dir_day_year + file_day + TString(buf) + file_day_end);
		cout << main_path + dir_day + TString(buf) + dir_day_year + file_day + TString(buf) + file_day_end << endl;
	}

	return t;
}

void fill_h(TChain* tchain1, TGraph2D* &histo) {

	Float_t driftTime, x, y, s1, s2;
	tchain1->SetBranchAddress("driftTime_ns", &driftTime);
	tchain1->SetBranchAddress("x_cm", &x);
	tchain1->SetBranchAddress("y_cm", &y);
	tchain1->SetBranchAddress("correctedS1area", &s1);
	tchain1->SetBranchAddress("correctedS2area", &s2);

	vector<Float_t> xs, ys, zs;
	for (int i = 0; i < tchain1->GetEntries(); i++) {
		tchain1->GetEntry(i);
		if (x * x + y * y < 4733.44 && driftTime * 0.0002 < 135.6 && driftTime * 0.0002 > 5
			&& s1 < 30 && TMath::Log10(s2 / s1) < 5) {
			xs.push_back(x);
			ys.push_back(y);
			zs.push_back(driftTime * 0.0002);
		}
	}
	histo = new TGraph2D(xs.size(), xs.data(), ys.data(), zs.data());
	histo->SetMarkerStyle(20);
}

void zones_(TGraph2D* &histo) {

	TCanvas* c1 = new TCanvas("c1", "Volume");

	gStyle->SetOptStat(0);

	c1->cd();
	gPad->SetTickx(2);
	gStyle->SetPalette(1);
	histo->Draw("pcol");
}


//Main
void VolumeHisto() {
	cout << "here" << endl;
	TGraph2D* h1;
	TChain* tchain1 = load_file();
	fill_h(tchain1, h1);
	zones_(h1);
	cout << "done" << endl;
}
