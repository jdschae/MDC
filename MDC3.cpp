#include "TROOT.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TMath.h"
#include "TString.h"
#include "TStyle.h"
#include "TMath.h"

#include <iostream>
#include <string>
#include <vector>

using namespace std;

const TString main_path = "/projecta/projectdirs/lz/data/mdc1-prod/LZAP-2.0.0-PHYSICS-2.0.0/";
const TString file_day = "lz_201707";
const TString file_day_end = "****_lzap.root";
const TString ttree_name = "Interactions";
const TString dir_day = "lz_mdc1_07-";
const TString dir_day_year = "-2017/";
const TString file_name = main_path + dir_day + file_day;

TChain* load_file() {
	cout << file_name << endl;
	TChain* t = new TChain(ttree_name);
	for (int i = 1; i <= 9; ++i) {
		t->Add(main_path + dir_day + "0" + TString(to_string(i)) + dir_day_year + file_day + "0" + TString(to_string(i)) + file_day_end);
		cout << main_path + dir_day + "0" + TString(to_string(i)) + dir_day_year + file_day + "0" + TString(to_string(i)) + file_day_end << endl;
	}
	for (int i = 10; i <= 28; ++i) {
		t->Add(main_path + dir_day + TString(i) + dir_day_year + file_day + TString(i) + file_day_end);
		cout << main_path + dir_day + TString(i) + dir_day_year + file_day + TString(i) + file_day_end << endl;
	}
	return t;
}

void fill_h(TChain* tchain1, TH1F* &h1, TH1F* &h2, TH1F* &h3, TH2F* &h4, TH1F* &h5, TGraph* &h6) {

	/*
	h = new TH1F("h2", "energy", 100, 0, 500);
	Float_t s1_, s2_;
	t->SetBranchAddress("S1area", &s1_); t1->SetBranchAddress("S2area", &s2_);
	for (int i = 0; i < t->GetEntries(); i++){
	  t->GetEntry(i);
	  h->Fill(s2_);
	}
	*/

	//h1 = new TH1F("h1", "energyNR", 100, 0, 10000);
	//h2 = new TH1F("h2", "energyER", 100, 0, 10000);
	//h3 = new TH1F("h3", "s1", 100, 0, 1000);
	//h4 = new TH2F("h4", "s2 v s1", 100, 0, 1000, 1000, 0, 2.5e6);
	//h5 = new TH1F("h5", "s2", 100, 0, 1000000);
	//("h6", "log10(s2/s1) vs s1", 100, 0, 30, 100, 1.8, 4.4);

	Float_t s1_, s2_, ER_, NR_, CX_, CY_, DT_;
	tchain1->SetBranchAddress("correctedS1area", &s1_);
	tchain1->SetBranchAddress("correctedS2area", &s2_);
	tchain1->SetBranchAddress("energyER", &ER_);
	tchain1->SetBranchAddress("energyNR", &NR_);
	tchain1->SetBranchAddress("x_cm", &CX_);
	tchain1->SetBranchAddress("y_cm", &CY_);
	tchain1->SetBranchAddress("driftTime_ns", &DT_);
	//tchain1->SetBranchAddress("correctedZ_cm", &CZ_);

	//Double_t* log_s2_s1_ = new Double_t[tchain1->GetEntries()];
	//Double_t* s1_s = new Double_t[tchain1->GetEntries()];
	//vector<Double_t> log_s2_s1_, s1_s;
	Int_t size = tchain1->GetEntries();
	vector<Float_t> log21, s1s;

	for (Int_t i = 0; i < size; ++i) {
		tchain1->GetEntry(i);
		//cout << CX_ * CX_ + CY_ * CY_ << endl;
		
		if (CX_ * CX_ + CY_ * CY_ < 4733.44) {
			if ( DT_ *0.0002 < 135.6 && DT_ *0.0002 > 5) {
				if (s1_ < 30) {
					if (TMath::Log10(s2_ / s1_) < 5) {
						log21.push_back(TMath::Log10(s2_ / s1_));
						s1s.push_back(s1_);
						cout << DT_ * 0.0002 << endl;
					}
				}
			}
		}
		//h6->SetPoint(i, s1_, );
		//Double_t x, y;
		//h6->GetPoint(i, x, y);
		//cout << x << endl;
		//cout << y << endl;
		//h1->Fill(ER_);
		//h2->Fill(NR_);
		//h3->Fill(s1_);
		//h4->Fill(s1_, s2_);
		//h5->Fill(s2_);
		//h6->Fill(s1_, TMath::Log10(s2_ / s1_));
	}
	cout << log21.size() << endl;
	h6 = new TGraph(log21.size(), s1s.data(), log21.data());
	h6->SetMarkerStyle(20);
	cout << "Filling done" << endl;
	//h6->SetTitle("log10(s2 / s1) vs s1");
}

/*
void graph_(TH1* h){
  TCanvas *c1 = new TCanvas("c1","c1");
  c1->SetCanvasSize(1500,1500);
  c1->SetWindowSize(500,500);
  h->Draw();
}
*/

void zones_(TH1F* &h1, TH1F* &h2, TH1F* &h3, TH2F* &h4, TH1F* &h5, TGraph* &h6) {


	//bin centers for the neutron band in S1

	Double_t NR_x[50] = { 0.42801413,   1.31712164,   2.1716916 ,   3.07923948,
			 4.07482036,   5.08701631,   6.11020936,   7.12770211,
			 8.15267463,   9.16977294,  10.19036486,  11.2141546 ,
			12.23658492,  13.25232501,  14.27364929,  15.29520378,
			16.31487693,  17.33854525,  18.36103642,  19.38276554,
			20.39394096,  21.41106522,  22.4394364 ,  23.45504492,
			24.4682034 ,  25.50069468,  26.51676325,  27.5354986 ,
			28.55844859,  29.58343803,  30.61302235,  31.64250835,
			32.63067313,  33.65162839,  34.67750675,  35.69830388,
			36.71695624,  37.73763737,  38.75543141,  39.79057647,
			40.82167715,  41.83143752,  42.83817489,  43.85180382,
			44.9240424 ,  45.8945477 ,  46.94612847,  47.9757289 ,
			48.98790448,  49.94147802 };

	//mean log(S2/S1) for neutron band:

	Double_t NR_mean[50] = { 3.94837995,  3.32639555,  3.12626475,  3.0057226 ,  2.93148023,
			2.88671468,  2.85243675,  2.82241094,  2.79625573,  2.77279779,
			2.75112324,  2.73085413,  2.71256789,  2.69552718,  2.68147891,
			2.66584453,  2.65270269,  2.63774042,  2.62672012,  2.61389524,
			2.60403464,  2.59359108,  2.5813734 ,  2.57429687,  2.5632845 ,
			2.55224456,  2.54826547,  2.53690016,  2.52997941,  2.52642763,
			2.51745848,  2.50906222,  2.50574694,  2.49268021,  2.48800073,
			2.4856213 ,  2.47847371,  2.47490008,  2.46059991,  2.46959762,
			2.45524656,  2.44753595,  2.44834233,  2.43553016,  2.43634545,
			2.4344676 ,  2.42308588,  2.41796162,  2.42620586,  2.41216355 };

	//1 sigma log(S2/S1) (I think) for neutron band:

	Double_t NR_sigma[50] = { 0.07279173,  0.19217566,  0.18572581,  0.17732698,  0.16467753,
			0.1514991 ,  0.13889602,  0.12990797,  0.12115851,  0.11639707,
			0.11181367,  0.10764515,  0.10595997,  0.10307257,  0.10093502,
			0.0987822 ,  0.0960274 ,  0.09431453,  0.09433907,  0.09098567,
			0.09016488,  0.08919846,  0.08808359,  0.08778795,  0.08563036,
			0.08659034,  0.08371712,  0.08220307,  0.08304506,  0.0781995 ,
			0.07991149,  0.07980904,  0.07485896,  0.07756339,  0.07481539,
			0.07388531,  0.07325604,  0.07719356,  0.07569493,  0.07361653,
			0.07127805,  0.07387061,  0.06880134,  0.0711063 ,  0.06760135,
			0.06897359,  0.07324009,  0.06603   ,  0.05880036,  0.06058131 };

	//bin centers for ER band in S1c:

	Double_t ER_x[50] = { 0,   1.30937217,   2.16815074,   3.08751726,
			 4.0814398 ,   5.09913341,   6.1220654 ,   7.13960709,
			 8.16385589,   9.18611611,  10.20556888,  11.21867523,
			12.24044352,  13.26527851,  14.2860154 ,  15.30501981,
			16.3285218 ,  17.33985026,  18.35880496,  19.38963793,
			20.40987388,  21.42777024,  22.45354119,  23.47101001,
			24.48948675,  25.51027966,  26.52351455,  27.55326568,
			28.56911475,  29.59195606,  30.60853236,  31.63521124,
			32.65674833,  33.67114919,  34.69280351,  35.71172749,
			36.73529338,  37.75190044,  38.77853197,  39.79612065,
			40.80873782,  41.83621772,  42.85640946,  43.87941018,
			44.89504875,  45.91968478,  46.93919721,  47.9620235 ,
			48.9789907 ,  50.00106467 };

	//mean log(S2/S1) for ER:

	Double_t ER_mean[50] = { 4,  3.7321955 ,  3.52221191,  3.38942066,  3.29781337,
			3.23481028,  3.18248494,  3.14022583,  3.1034032 ,  3.07640889,
			3.05315865,  3.02874918,  3.01345797,  2.99467839,  2.98058241,
			2.96959624,  2.95649981,  2.94731592,  2.93668489,  2.92944257,
			2.92014787,  2.91118678,  2.90627886,  2.90269459,  2.89606644,
			2.89153475,  2.88702234,  2.88192268,  2.87606932,  2.87330275,
			2.87175176,  2.86868645,  2.8652117 ,  2.86290918,  2.86018089,
			2.85800389,  2.85510032,  2.85636107,  2.85230776,  2.84887925,
			2.84724657,  2.84710578,  2.84746516,  2.84377469,  2.84209227,
			2.83974685,  2.83890986,  2.83779666,  2.8365254 ,  2.83563847 };

	// 1 sigma log(S2/S1) for ER:

	Double_t ER_sigma[50] = { 0.1,  0.14030014,  0.13234417,  0.12443383,  0.11668275,
			0.11211814,  0.11171576,  0.1100494 ,  0.11002637,  0.10970432,
			0.11181609,  0.11018967,  0.10914204,  0.11023227,  0.10744612,
			0.1089232 ,  0.10815561,  0.10796495,  0.10861617,  0.10977526,
			0.1094979 ,  0.11093083,  0.11127309,  0.11314772,  0.11321932,
			0.11450008,  0.11575146,  0.11522213,  0.11649877,  0.11815586,
			0.11777488,  0.12039718,  0.12135455,  0.12155193,  0.12034387,
			0.12288927,  0.12393673,  0.12365863,  0.12534466,  0.12627122,
			0.12693353,  0.12620801,  0.13006601,  0.12906292,  0.12868885,
			0.1289055 ,  0.13168046,  0.12945248,  0.1320064 ,  0.13237906 };

	Double_t ER_sp[50] = {};
	Double_t ER_sm[50] = {};
	Double_t NR_sp[50] = {};
	Double_t NR_sm[50] = {};

	for (int i = 0; i < 50; i++) {
		ER_sp[i] = ER_mean[i] + ER_sigma[i];
		ER_sm[i] = ER_mean[i] - ER_sigma[i];
		NR_sp[i] = NR_mean[i] + NR_sigma[i];
		NR_sm[i] = NR_mean[i] - NR_sigma[i];
	}

	/*
	c1 = new TCanvas("c1", "ER");
	c2 = new TCanvas("c2", "NR");
	c3 = new TCanvas("c3", "s1");
	c4 = new TCanvas("c4", "s2 v s1");
	c5 = new TCanvas("c5", "s2");
	*/
	c6 = new TCanvas("c6", "log10(s2/s1) vs s1");
	gStyle->SetOptStat(0);
	
	/*
	c1->cd();
	gPad->SetTickx(2);
	h1->Draw();

	c2->cd();
	gPad->SetTickx(2);
	gPad->SetTicky(2);
	h2->GetYaxis()->SetLabelOffset(0.01);
	h2->Draw();

	c3->cd();
	h3->Draw();

	c4->cd();
	gPad->SetTicky(2);
	h4->Draw();

	c5->cd();
	h5->Draw();
	*/
	cout << "drawing" << endl;
	c6->cd();
	h6->Draw("ap");
	h6->GetXaxis()->SetRangeUser(0,30);
	h6->GetYaxis()->SetRangeUser(1.8,4.2);
	h6->SetTitle("MDC ER/NR Discrimination; S1c [phd]; log10(S2c/S1c)");
	//c6->SetXaxisTile("S1");
	//c6->SetYaxisTile("log10(S2/S1)");
	
	TGraph *gr1 = new TGraph(50, NR_x, NR_mean);
	gr1->SetLineColor(2);
	gr1->Draw("Same");
	c6->Update();
	TGraph *gr3 = new TGraph(50, NR_x, NR_sp);
	gr3->SetLineColor(2);
	gr3->SetLineStyle(2);
	gr3->Draw("Same");
	c6->Update();
	TGraph *gr4 = new TGraph(50, NR_x, NR_sm);
	gr4->SetLineColor(2);
	gr4->SetLineStyle(2);
	gr4->Draw("Same");
	c6->Update();
	TGraph *gr2 = new TGraph(50, ER_x, ER_mean);
	gr2->SetLineColor(4);
	gr2->Draw("Same");
	c6->Update();
	TGraph *gr5 = new TGraph(50, ER_x, ER_sp);
	gr5->SetLineColor(4);
	gr5->SetLineStyle(2);
	gr5->Draw("Same");
	c6->Update();
	TGraph *gr6 = new TGraph(50, ER_x, ER_sm);
	gr6->SetLineColor(4);
	gr6->SetLineStyle(2);
	gr6->Draw("Same");
	c6->Update();
	
}


//Main
void MDC3() {
	cout << "here" << endl;
	TH1F* h1;
	TH1F* h2;
	TH1F* h3;
	TH2F* h4;
	TH1F* h5;
	TGraph* h6;
	TCanvas* c1;
	TChain* tchain1 = load_file();
	fill_h(tchain1, h1, h2, h3, h4, h5, h6);
	zones_(h1, h2, h3, h4, h5, h6);
	cout << "done" << endl;
}
