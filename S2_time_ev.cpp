#include "TROOT.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TMath.h"
#include "TString.h"
#include "TStyle.h"

#include <iostream>
#include <string>

using namespace std;

const TString main_path = "/projecta/projectdirs/lz/data/mdc1-prod/LZAP-2.0.0-PHYSICS-2.0.0/";
const TString file_day = "lz_201707";
const TString file_day_end = "****_lzap.root";
const TString ttree_name = "Interactions";
const TString dir_day = "lz_mdc1_07-";
const TString dir_day_year = "-2017/";
const TString file_name = main_path + dir_day + file_day;

TChain* load_file() {
	cout << file_name << endl;
	TChain* t = new TChain(ttree_name);
	for (int i = 1; i <= 9; ++i) {
		t->Add(main_path + dir_day + "0" + TString(to_string(i)) + dir_day_year + file_day + "0" + TString(to_string(i)) + file_day_end);
	}
	for (int i = 10; i <= 30; ++i) {
		t->Add(main_path + dir_day + TString(to_string(i)) + dir_day_year + file_day + TString(to_string(i)) + file_day_end);
	}
	return t;
}

void fill_h(TChain* tchain1, TH2F* &h1) {


	h1 = new TH2F("h4", "s2 v time", 720, 0, 720, 100, 0, 18);

	Float_t s2_;
	tchain1->SetBranchAddress("correctedS2area", &s2_);
	TString name = "";
	int time = -1;
	for (int i = 0; i < tchain1->GetEntries(); i++) {
		tchain1->GetEntry(i);
		if (name != tchain1->GetFile()->GetName()) {
			++time;
			name = tchain1->GetFile()->GetName();
		}
		h1->Fill(time, log(s2_));
	}
	cout << time << endl;
}

void zones_(TH2F* &h1) {
	TCanvas* c1 = new TCanvas("c1", "s2 vs time");

	gStyle->SetOptStat(0);

	c1->cd();
	gPad->SetTickx(2);
	h1->Draw("COLZ");
}


//Main
void S2_time_ev() {
	cout << "here" << endl;
	TH2F* h1;
	TChain* tchain1 = load_file();
	fill_h(tchain1, h1);
	zones_(h1);
	cout << "done" << endl;
}
