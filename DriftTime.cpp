#include "TROOT.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TMath.h"
#include "TString.h"
#include "TStyle.h"
#include "TMath.h"

#include <iostream>
#include <string>

using namespace std;

const TString main_path = "/projecta/projectdirs/lz/data/mdc1-prod/LZAP-2.0.0-PHYSICS-2.0.0/";
const TString file_day = "lz_201707";
const TString file_day_end = "****_lzap.root";
const TString ttree_name = "Interactions";
const TString dir_day = "lz_mdc1_07-";
const TString dir_day_year = "-2017/";
const TString file_name = main_path + dir_day + file_day;

TChain* load_file() {
	cout << file_name << endl;
	TChain* t = new TChain(ttree_name);

	for (int i = 1; i <= 30; ++i) {
		char buf[3];
		sprintf(buf, "%02d", i);
		t->Add(main_path + dir_day + TString(buf) + dir_day_year + file_day + TString(buf) + file_day_end);
		cout << main_path + dir_day + TString(buf) + dir_day_year + file_day + TString(buf) + file_day_end << endl;
	}

	return t;
}

void fill_h(TChain* tchain1, TH1F* &histo) {

	histo = new TH1F("h1", "DriftTime", 1000, -1000, 100000);


	Float_t driftTime;
	tchain1->SetBranchAddress("driftTime_ns", &driftTime);

	for (int i = 0; i < tchain1->GetEntries(); i++) {
		tchain1->GetEntry(i);
		histo->Fill(driftTime);
	}
}

void zones_(TH1F* &histo) {

	TCanvas* c1 = new TCanvas("c1", "drift");

	gStyle->SetOptStat(0);

	c1->cd();
	gPad->SetTickx(2);
	histo->Draw();
}


//Main
void DriftTime() {
	cout << "here" << endl;
	TH1F* h1;
	TChain* tchain1 = load_file();
	fill_h(tchain1, h1);
	zones_(h1);
	cout << "done" << endl;
}
